﻿using SSTemplate.Domain.Entities.Models;
using SSTemplate.Domain.Interfaces.Infrastructure;
using System.Threading.Tasks;

namespace SSTemplate.Domain.Interfaces
{
    public interface ITestModelRepository : IRepository<TestModel>
    {
        Task<TestModel> Test();
    }
}
