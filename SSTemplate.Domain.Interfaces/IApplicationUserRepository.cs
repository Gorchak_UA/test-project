﻿using SSTemplate.Domain.Entities.Models;
using SSTemplate.Domain.Interfaces.Infrastructure;
using System.Threading.Tasks;
using SSTemplate.Domain.Entities.Infrastructure;
using SSTemplate.Domain.Entities.ViewModels;
using SSTemplate.Domain.Entities.Enums;

namespace SSTemplate.Domain.Interfaces
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        Task<ShortUser> GetCurrentUser(string userId);
    }
}
