﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSTemplate.Domain.Entities.ViewModels;
using SSTemplate.Domain.Entities.Struct;

namespace SSTemplate.Services.Interfaces
{
    public interface IAccountService
    {
        Task<ServiceResult> Register(RegisterBindingModel model);
    }
}