﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSTemplate.Domain.Entities.Infrastructure;
using SSTemplate.Domain.Entities.Struct;
using SSTemplate.Domain.Entities.ViewModels;

namespace SSTemplate.Services.Interfaces
{
    public interface IUserService
    {
        Task<ShortUser> GetCurrentUser(string id);
    }
}
