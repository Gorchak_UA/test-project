﻿using System.Threading.Tasks;

namespace SSTemplate.Services.Interfaces.RealtimeServices
{
    public interface ITestRealtimeService
    {
        Task Method();
    }
}
