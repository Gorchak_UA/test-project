﻿using SSTemplate.Domain.Entities.Models;
using SSTemplate.Domain.Interfaces;
using SSTemplate.Domain.Interfaces.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSTemplate.Domain.Entities.Infrastructure;
using SSTemplate.Domain.Entities.ViewModels;
using MongoDB.Driver;
using Microsoft.AspNet.Identity;
using SSTemplate.Domain.Entities.Enums;

namespace SSTemplate.Infrastructure.Data
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        private readonly ProjectionDefinition<ApplicationUser, ShortUser> ShortUserProjection = Builders<ApplicationUser>.Projection.Expression(x => new ShortUser()
        {
            Id = x.Id,
            Email = x.Email,
            FirstName = x.FirstName,
            LastName = x.LastName,
            Image = x.Image,
            PhoneNumber = x.PhoneNumber,
            Roles = x.Roles
        });

        public async Task<ShortUser> GetCurrentUser(string userId)
        {
            return await base.FindOneAsync(userId, ShortUserProjection);
        }


    }
}
