﻿using SSTemplate.Domain.Entities.Models;
using SSTemplate.Domain.Interfaces;
using SSTemplate.Domain.Interfaces.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSTemplate.Infrastructure.Data
{
    public class TestModelRepository : GenericRepository<TestModel>, ITestModelRepository
    {
        public TestModelRepository() : base()
        {
        }

        public async Task<TestModel> Test()
        {
            var item = new TestModel
            {
                First = "1",
                Second = 2
            };

            var result = await base.InsertOneAsync(item);

            return item;
        }
    }
}
