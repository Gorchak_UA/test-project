﻿using Microsoft.AspNet.Identity;
using SSTemplate.Domain.Entities;
using SSTemplate.Domain.Entities.ViewModels;
using SSTemplate.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using SSTemplate.Domain.Interfaces;
using System.Net;
using SSTemplate.Domain.Entities.Models;
using System.Configuration;
using SSTemplate.Domain.Entities.Infrastructure;
using System.Net.Http;

namespace SSTemplate.Infrastructure.Business
{
    public class SettingsService : ISettingsService
    {

        #region Initialization
        private ApplicationUserManager _userManager;


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        public async Task<IdentityResult> ChangePassword(string userId, ChangePasswordBindingModel model)
        {
            IdentityResult result = await UserManager.ChangePasswordAsync(userId, model.OldPassword, model.NewPassword);

            return result;
        }


    }
}
