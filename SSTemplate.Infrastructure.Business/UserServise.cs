﻿using MongoDB.Driver;
using SSTemplate.Domain.Entities.Models;
using SSTemplate.Domain.Interfaces;
using SSTemplate.Services.Interfaces;
using System.Threading.Tasks;
using SSTemplate.Domain.Entities.ViewModels;
using SSTemplate.Domain.Entities.Struct;
using Microsoft.AspNet.Identity;
using SSTemplate.Domain.Entities.Infrastructure;
using System;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using SSTemplate.Domain.Entities.Enums;
using System.Web.Http;
using SSTemplate.Infrastructure.Data.Utility.AzureBlob;
using SSTemplate.Domain.Entities.Dictionaries;
using System.Linq;
using AspNet.Identity.MongoDB;

namespace SSTemplate.Infrastructure.Business
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private ServiceResult<ApplicationUser> _serviceResult;

        public UserService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        #region Initialization
        private ApplicationUserManager _userManager;
        //private ApplicationRoleManager _roleManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //public ApplicationRoleManager RoleManager
        //{
        //    get
        //    {
        //        return _roleManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
        //    }
        //    private set
        //    {
        //        _roleManager = value;
        //    }
        //}
        #endregion

        public async Task<ShortUser> GetCurrentUser(string id)
        {
            var result = await _userRepository.GetCurrentUser(id);
            result.Image = UploadImageProperties.BlobAdress + result.Image;
            return result;
        }
    }
}
