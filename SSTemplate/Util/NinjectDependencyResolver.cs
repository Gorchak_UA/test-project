﻿using Ninject;
using SSTemplate.Domain.Interfaces;
using SSTemplate.Infrastructure.Business;
using SSTemplate.Infrastructure.Data;
using SSTemplate.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SSTemplate.Infrastructure.Business.RealtimeServices;
using SSTemplate.Infrastructure.Business.Hubs;
using SSTemplate.Services.Interfaces.RealtimeServices;

namespace SSTemplate.Web.Util
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            //hub
            kernel.Bind<UserActivityHub>().To<UserActivityHub>().InSingletonScope();

            //realtime services
            kernel.Bind<ITestRealtimeService>().To<TestRealtimeService>();

            //configuration

            // repository
            kernel.Bind<ITestModelRepository>().To<TestModelRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();


            // service
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IAccountService>().To<AccountService>();
            kernel.Bind<ISettingsService>().To<SettingsService>();
        }
    }
}