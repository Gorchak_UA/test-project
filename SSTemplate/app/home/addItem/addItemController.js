﻿(function () {
    'use strict';

    function addItemController($state, sayerService) {
        var ctrl = this;

        ctrl.addItem = function (form) {
            var itemId;
            var isStartPush = false;

            //sayerService.getItems().then(function (items) {
            //    if (items) {
            //        itemId = items.length;
            //    } else {
            //        itemId = 0;
            //    }
            //    debugger;
            //    return items;
            //});


            ctrl.item = {
                itemComments: [],
                itemTitle: ''
            };
            ctrl.item.itemTitle = ctrl.newItemName;

            sayerService.addItem(ctrl.item);

            ctrl.newItemName = '';
            $state.go('itemsList', {}, { reload: true });



        }
    }

    addItemController.$inject = ['$state', 'sayerService'];

    angular.module('ss.home')
        .component('addItem', {
            templateUrl: '/app/home/addItem/addItem.html',
            controller: addItemController
        })
})()
