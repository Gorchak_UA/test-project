﻿(function () {
    'use strict';

    function homeController($state) {
        var ctrl = this;
    }

    homeController.$inject = ['$state'];

    angular.module('ss.home')
        .component('homePage', {
            templateUrl: '/app/home/home.html',
            controller: homeController
        })
})()
