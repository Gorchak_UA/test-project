﻿(function () {
    'use strict';

    function itemOpenController($state, localStorageService, sayerService, $http, $q) {
        var ctrl = this;
        ctrl.addCommentText = '';



        ctrl.sayerService = sayerService;

        ctrl.itemsList = localStorageService.get('itemsList');

        console.log('itemsList', ctrl.itemsList);

        ctrl.setComment = function (form, id) {
            ctrl.commentModel = {
                commentText: '',
                userPhoto: ''
            }
            ctrl.commentModel.commentText = ctrl.addCommentText;
            ctrl.commentModel.userPhoto = 'http://icons.iconarchive.com/icons/custom-icon-design/silky-line-user/256/user-2-icon.png';

            sayerService.addComment(ctrl.commentModel, id)
                .then(function (test) {
                    alert('Success');
                });
            ctrl.addCommentText = '';

            $state.go('itemOpen', {}, { reload: true });

        }
    }

    itemOpenController.$inject = ['$state', 'localStorageService', 'sayerService', '$http', '$q'];

    angular.module('ss.home')
        .component('itemOpen', {
            templateUrl: '/app/home/itemOpen/itemOpen.html',
            controller: itemOpenController,
            bindings: {
                comments:'<'
            }
        })
})()
