﻿(function () {
    angular.module('ss.home', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('home', {
                    url: "/",
                    template: '<home-page></home-page>',
                    data: {
                        pageTitle: 'Home',
                        noLogin: true
                    }
                    //resolve: {
                    //    authorize: ['authorizationService', function (authorizationService) {
                    //        return authorizationService.authorize();
                    //    }]
                    //}
                });

            $stateProvider
                .state('addItem', {
                    url: "/addItem",
                    template: '<add-item></add-item>',
                    data: {
                        pageTitle: 'Add new Item',
                        noLogin: true
                    }
                })

            $stateProvider
                .state('itemOpen', {
                    url: "/itemOpen/:id",
                    template: '<item-open comments=$resolve.comments></item-open>',
                    data: {
                        pageTitle: 'Item page',
                        noLogin: true
                    },
                    resolve: {
                        comments: ['sayerService', '$stateParams', function (sayerService, $stateParams) {
                            var id = $stateParams.id;

                            return sayerService.getItem(id);
                            }
                        ]
                    }
                })

            $stateProvider
                .state('itemsList', {
                    url: "/itemsList",
                    template: '<items-list items=$resolve.items></items-list>',
                    data: {
                        pageTitle: 'Items list',
                        noLogin: true
                    },
                    resolve: {
                        items: ['sayerService', '$stateParams', function (sayerService, $stateParams) {
                            var id = $stateParams.id;

                            return sayerService.getItems();
                        }
                        ]
                    }
                })
        }])
})();