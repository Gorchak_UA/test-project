﻿(function () {
    'use strict';

    function itemsListController($state, sayerService, localStorageService) {
        var ctrl = this;
        ctrl.sayerService = sayerService;

        ctrl.itemsList = localStorageService.get('itemsList');
    

        //ctrl.sayerItemsList = ctrl.sayerService.sayerItemsList;

        ctrl.deleteItem = function (id) {
            sayerService.deleteItem(id);
            $state.go('itemsList', {}, { reload: true });
        }
    }

    itemsListController.$inject = ['$state', 'sayerService', 'localStorageService'];

    angular.module('ss.home')
        .component('itemsList', {
            templateUrl: '/app/home/itemsList/itemsList.html',
            controller: itemsListController,
            bindings: {
                items: '<'
            }
        })
})()
