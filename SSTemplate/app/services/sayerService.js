﻿(function () {
    'use strict';

    function sayerService($http, $rootScope, $state, localStorageService, toastr, $q) {
        var vm = this;

        vm.getItem = function (id) {
            var deferred = $q.defer();
            var deferredBool = false;

            var itemslist = localStorageService.get('itemsList');

            for (var i = 0; i < itemslist.length; i++) {
                if (itemslist[i].id == id) {
                    deferred.resolve(itemslist[i]);
                    var deferredBool = true;
                }
            }

            if (!deferredBool) {
                alert('Item is not found');
                $state.go('itemsList');
            }
            return deferred.promise;
        }

        vm.getItems = function () {
            var deferred = $q.defer();

            var itemslist = localStorageService.get('itemsList');

            if (itemslist == null) {
                itemslist = [];
                localStorageService.set('itemsList', itemslist);
            }

            deferred.resolve(itemslist);
            return deferred.promise;
        }



        vm.addComment = function (form, id) {
            var deferred = $q.defer();

            var itemslist = localStorageService.get('itemsList');

            itemslist.forEach(function (item, i, itemslist) {
                if (item.id == id) {
                    itemslist[i].itemComments.push(form);
                    localStorageService.set('itemsList', itemslist);
                }
            });


          

            localStorageService.set('itemsList', itemslist);


            return deferred.promise;
        }

        vm.deleteItem = function (id) {
            var deferred = $q.defer();

            var itemslist = localStorageService.get('itemsList');

            itemslist.forEach(function (item, i, itemslist) {
                if (item.id == id) {

                    itemslist.splice(i, 1);
                    localStorageService.set('itemsList', itemslist);

                }
            });



            return deferred.promise;
        }


        function getRandomArbitary(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }

        function getItemId(items) {
            var itemId = 0;

            var test = items;
            var itemId = getRandomArbitary(items.length, items.length + 100);

            if (items.length > 0) {
                items.forEach(function (element) {
                    if (element.id == itemId) {
                        itemId = getRandomArbitary(itemId, itemId + 100);
                        localStorageService.set('itemsList', itemslist);
                    }
                });
            }


            return itemId;
        }

        vm.addItem = function (item) {
            var deferred = $q.defer();

            vm.getItems().then(function (items) {

                var itemId = getItemId(items);
                item.id = itemId;
                items.push(item);
                localStorageService.set('itemsList', items);
            });


            return deferred.promise;
        }
    };

    sayerService.$inject = ['$http', '$rootScope', '$state', 'localStorageService', 'toastr', '$q'];

    angular
        .module('sayer.service', [])
        .service('sayerService', sayerService);

})();