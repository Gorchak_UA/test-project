﻿(function () {
    angular.module('ss.access', [])
        .constant('accessConfig', {
            notAuthState: 'home',
            authState: 'home',
            accessdeniedState: 'accessdenied'
        })
        .run(['$rootScope', '$state', 'accessConfig', run]);

    function run($rootScope, $state, accessConfig) {
        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
            var noLogin = false;

            if (toState.data != null)
                if (toState.data.noLogin)
                    noLogin = toState.data.noLogin;

            if (noLogin)
                return; // no need to redirect 
        });
    }
})();