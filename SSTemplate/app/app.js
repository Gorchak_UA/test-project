﻿/* App Module */
angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'ss.error',
    'ss.access',
    'ss.loading',
    'ss.home',
    'sayer.service',
    'realtime.service',
    'ngMessages',
    'interceptorRequestError',
    'toastr',
    'LocalStorageModule',
    'ngFileUpload',
    'vcRecaptcha',
])
    .constant('appConfig', {
        'urlAzureBlob': 'https://test.blob.core.windows.net/'
    })
    .config(['$sceDelegateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'toastrConfig', '$stateProvider', 'accessConfig', 'errorConfig', '$qProvider',
        function ($sceDelegateProvider, $urlRouterProvider, $locationProvider, $httpProvider, toastrConfig, $stateProvider, accessConfig, errorConfig, $qProvider) {

            angular.extend(toastrConfig, {
                allowHtml: true,
                newestOnTop: true,
                progressBar: true,
                preventOpenDuplicates: true
            });

            errorConfig.exeptUrlToastr.push('/api/v1/account/register');

            $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            $urlRouterProvider.otherwise("/error/not-found");

            $sceDelegateProvider.resourceUrlWhitelist([
                'self'
            ]);
        }])
    .run(['$http', '$rootScope', '$state', 'localStorageService', function ($http, $rootScope, $state, localStorageService) {
        $rootScope.$state = $state;
    }]);
