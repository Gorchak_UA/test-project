﻿using SSTemplate.Domain.Entities.Enums;
using SSTemplate.Domain.Entities.Struct;
using SSTemplate.Web.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SSTemplate.Web.Controllers.ApiControllers
{
    public class CustomApiController : ApiController
    {
        public IHttpActionResult ServiceResult<T>(ServiceResult<T> source) where T : class
        {
            if (source.Success)
            {
                return Ok(source.Result);
            }
            else
            {
                return new ErrorResult(source, Request);
            }
        }

        public IHttpActionResult ServiceResult(ServiceResult source)
        {
            if (source.Success)
            {
                return Ok();
            }
            else
            {
                return new ErrorResult(source, Request);
            }
        }
    }
}
