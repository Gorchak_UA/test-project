﻿using System.Web.Mvc;

namespace SSTemplate.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/Shared/_LoadingPage.cshtml");
        }
    }
}